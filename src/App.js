import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Users from "./pages/Users";
import ContactUs from "./pages/ContactUs";
import Products from "./pages/Projucts";
import NotFound from "./pages/NotFound";
import UserDashboard from "./pages/UserDashboard";
import UserProfile from "./pages/UserProfile";
import Protected from "./components/Protected";
import React,{useState} from "react";
import NavBar from "./components/NavBar";
function App() {
  const [isSignIn, setIsSignIn] = useState(false)
  return (
    <>
      {/* Protected or private routes  */}
      <div>
        <NavBar/>
      </div>
      
      <Routes>
        <Route index element={<Home />} />
        <Route path="/home" element={<Home />} />

        {/*  Nested Routes  */}
        <Route path="user">
          <Route index element={<Users />} />
          <Route path="profile" element={<UserProfile />} />
          <Route path="dashboard" element={<UserDashboard />} />
          {/* URL Params  */}
          <Route path=":id" element={<UserProfile />} />
        </Route>

        <Route path="/contactus" element={<ContactUs />} />
        
        <Route
          path="/products"
          element={
            <Protected isSignedIn={isSignIn}>
              <Products />
            </Protected>
          }
        />
        <Route path="*" element={<NotFound />} />
      </Routes>


      <button onClick={()=> setIsSignIn(!isSignIn)} className="btn btn-warning">{(isSignIn? "Logout ": "Login")} </button>
    </>

    // user/
    // user/profile
    // user/addnew
    // user/13
  );
}

export default App;
