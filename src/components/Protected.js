import React from 'react'
import { Navigate } from 'react-router-dom';
function Protected({isSignedIn, children}) {
    
    console.log("Is Sign In : ", isSignedIn)
    if(!isSignedIn){
       return  <Navigate to="/home" replace />
    }

     
    return children  ; 
}

export default Protected