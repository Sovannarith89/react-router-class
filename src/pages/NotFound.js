import React from 'react'
import notFoundImage from '../images/notFound.png'
function NotFound() {
  return (
    <div className='container text-center mt-5'>
        <img style={{objectFit:'contain'}} width="400px" src={notFoundImage} alt="not found image " />
        <h1 className='text-danger display-1 fw-bold'>404</h1>
        <h2>Not Found </h2>

    </div>
  )
}

export default NotFound